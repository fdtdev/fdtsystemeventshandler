using UnityEditor;
using com.FDT.GameEvents.Editor;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler.Editor
{
	[CustomEditor(typeof(SceneLoadGameEvent))]
	public class SceneLoadGameEventEditor : GameEvent2Editor<Scene, LoadSceneMode>
	{
	}
}
