using UnityEngine;
using UnityEditor;
using com.FDT.SystemEventsHandler;

namespace com.FDT.SystemEventsHandler.Editor
{
	[CustomPropertyDrawer(typeof(SceneUnloadGameEventHandle))]
	public class SceneUnloadGameEventHandleDrawer : PropertyDrawer 
	{
		public override void OnGUI (Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty (position, label, property);
			EditorGUI.PropertyField (position, property.FindPropertyRelative("_Event"), label);
			EditorGUI.EndProperty ();
		}
	}
}
