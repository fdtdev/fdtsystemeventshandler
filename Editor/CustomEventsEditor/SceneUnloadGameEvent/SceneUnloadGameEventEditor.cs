using UnityEngine;
using UnityEditor;
using com.FDT.GameEvents.Editor;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler.Editor
{
	[CustomEditor(typeof(SceneUnloadGameEvent))]
	public class SceneUnloadGameEventEditor : GameEvent1Editor<Scene>
	{
		public override void OnInspectorGUI()
		{
			base.OnInspectorGUI();

			SceneUnloadGameEvent _cTarget = target as SceneUnloadGameEvent;
			GUI.enabled = Application.isPlaying;

			if (GUILayout.Button("Raise"))
				_cTarget.Raise();
			if (Application.isPlaying) {
				EditorGUILayout.Space ();
				EditorGUILayout.LabelField ("Listeners");
				var list = (target as SceneUnloadGameEvent).GetEventListeners();
				foreach (var l in list) {
					var e = l.GetExecute ();
					if (e != null) {
                        DrawExecute(e.Target);
					} else {
							GameObject tgo = l.GetGameObject ();
						if (tgo != null) {
                            DrawGameObject(tgo);
						}
					}
				}
			}
		}
	}
}
