using com.FDT.GameEvents.Editor;
using UnityEditor;

namespace com.FDT.SystemEventsHandler.Editor
{
	[CustomEditor(typeof(FocusModeChangedGameEvent))]
	public class FocusModeChangedGameEventEditor : GameEvent1Editor<AppFocusMode>
	{
	}
}
