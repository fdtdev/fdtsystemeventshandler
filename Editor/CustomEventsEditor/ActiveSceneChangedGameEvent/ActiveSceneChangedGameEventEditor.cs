using UnityEditor;
using com.FDT.GameEvents.Editor;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler.Editor
{
	[CustomEditor(typeof(ActiveSceneChangedGameEvent))]
	public class ActiveSceneChangedGameEventEditor : GameEvent2Editor<Scene, Scene>
	{
	}
}
