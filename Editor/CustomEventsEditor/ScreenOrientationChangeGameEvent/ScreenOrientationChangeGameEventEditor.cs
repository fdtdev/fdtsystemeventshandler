using UnityEngine;
using UnityEditor;
using com.FDT.GameEvents.Editor;

namespace com.FDT.SystemEventsHandler.Editor
{
	[CustomEditor(typeof(ScreenOrientationChangeGameEvent))]
	public class ScreenOrientationChangeGameEventEditor : GameEvent1Editor<ScreenOrientation>
	{
	}
}
