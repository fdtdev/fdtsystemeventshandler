﻿using UnityEngine;
using com.FDT.GameEvents;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler
{ 
    public class SystemEventsHandler : MonoBehaviour {

        [Header("Application"), SerializeField] protected GameEvent _lowMemoryEvt;
        [SerializeField] protected GameEvent _quittingEvt;
        [Header("SceneManager"), SerializeField] protected SceneLoadGameEvent _loadSceneEvt;
        [SerializeField] protected SceneUnloadGameEvent _unloadSceneEvt;
        [SerializeField] protected ActiveSceneChangedGameEvent _activeSceneChangedEvt;
        [Header("Focus and screen"), SerializeField] protected FocusModeChangedGameEvent _focusModeChangedEvt;
        [SerializeField] protected ScreenOrientationChangeGameEvent _screenOrientationEvt;

        public AppFocusMode focus = AppFocusMode.FOCUSED;
        protected  bool paused = false;
        protected bool focused = true;
        [SerializeField] protected ScreenOrientation _orientation;

        void Awake () {
            Application.lowMemory += HandleLowMemory;
            Application.quitting += HandleQuitting;
            SceneManager.sceneLoaded += HandleSceneLoaded;
            SceneManager.sceneUnloaded += HandleSceneUnloaded;
            SceneManager.activeSceneChanged += HandleActiveSceneChanged;
	    }
        private void Update()
        {
            if (Screen.orientation != _orientation)
            {
                _orientation = Screen.orientation;
                _screenOrientationEvt.Raise(_orientation);
            }
        }
        void OnApplicationFocus(bool focusStatus)
        {
            if (focused != focusStatus)
            { 
                focused = focusStatus;
                SetFocusMode();
            }
            
            Debug.Log("OnApplicationFocus " + focusStatus);
        }

        void OnApplicationPause(bool pauseStatus)
        {
            if (paused != pauseStatus)
            { 
                paused = pauseStatus;
                SetFocusMode();
            }
            
            Debug.Log("OnApplicationPause " + pauseStatus);
        }
        void SetFocusMode()
        {
            bool changed = false;
            if (focused && !paused)
            {
                if (focus != AppFocusMode.FOCUSED)
                {
                    changed = true;
                    focus = AppFocusMode.FOCUSED;
                }
            }
            else if (!focused && !paused)
            { 
                if (focus != AppFocusMode.NOT_FOCUSED)
                {
                    changed = true;
                    focus = AppFocusMode.NOT_FOCUSED;
                }
            }
            else if (paused)
            { 
                if (focus != AppFocusMode.PAUSED)
                {
                    changed = true;
                    focus = AppFocusMode.PAUSED;
                }
            }
            if (changed)
                _focusModeChangedEvt.Raise(focus);
        }
        void HandleActiveSceneChanged(Scene current, Scene next)
        {
            if (_activeSceneChangedEvt!=null)
                _activeSceneChangedEvt.Raise(current, next);
        }
        void HandleSceneLoaded(Scene s, LoadSceneMode m)
        {
            if (_loadSceneEvt!= null)
                _loadSceneEvt.Raise(s, m);
        }

        void HandleSceneUnloaded(Scene s)
        {
            if (_unloadSceneEvt!=null)
                _unloadSceneEvt.Raise(s);
        }
        void HandleLowMemory()
        {
            if (_lowMemoryEvt!=null)
                _lowMemoryEvt.Raise();
        }
        void HandleQuitting()
        {
            if (_quittingEvt != null)
                _quittingEvt.Raise();
        }
        void OnDestroy () {
            Application.quitting -= HandleQuitting;
            SceneManager.activeSceneChanged -= HandleActiveSceneChanged;
            SceneManager.sceneUnloaded -= HandleSceneUnloaded;
            SceneManager.sceneLoaded -= HandleSceneLoaded;
            Application.lowMemory -= HandleLowMemory;
        }
    }
}