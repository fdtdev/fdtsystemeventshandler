﻿namespace com.FDT.SystemEventsHandler
{
    public enum AppFocusMode
    {
        FOCUSED = 0, NOT_FOCUSED = 1, PAUSED = 2
    }
}