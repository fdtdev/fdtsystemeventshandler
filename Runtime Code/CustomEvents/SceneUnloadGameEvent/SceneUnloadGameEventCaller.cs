using com.FDT.GameEvents;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler
{
	public class SceneUnloadGameEventCaller: GameEvent1Caller<Scene, SceneUnloadGameEvent>
	{
	}
}
