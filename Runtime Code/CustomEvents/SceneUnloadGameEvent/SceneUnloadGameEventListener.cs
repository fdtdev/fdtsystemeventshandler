using com.FDT.GameEvents;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler
{
	public class SceneUnloadGameEventListener : GameEvent1Listener<Scene, SceneUnloadGameEvent, SceneUnloadGameEventListener.UEvt>
	{
        [System.Serializable]
        public class UEvt : UnityEvent<Scene>
        {
        }
	}
}
