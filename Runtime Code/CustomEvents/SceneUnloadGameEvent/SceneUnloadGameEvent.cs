using UnityEngine;
using com.FDT.GameEvents;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler
{
    [CreateAssetMenu(menuName = "FDT/GameEvents/SystemEventsHandler/SceneUnloadGameEvent")]
	public class SceneUnloadGameEvent : GameEvent1<Scene>
	{
        public override string arg0label
        {
            get { return "Scene"; }
        }
	}
}
