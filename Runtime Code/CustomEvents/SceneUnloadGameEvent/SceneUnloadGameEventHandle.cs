using com.FDT.GameEvents;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler
{
	[System.Serializable]
	public class SceneUnloadGameEventHandle: GameEvent1Handle<Scene, SceneUnloadGameEvent>
	{
	}
}
