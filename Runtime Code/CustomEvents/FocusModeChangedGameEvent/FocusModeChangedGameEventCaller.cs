using com.FDT.GameEvents;

namespace com.FDT.SystemEventsHandler
{
    public class FocusModeChangedGameEventCaller : GameEvent1Caller<AppFocusMode, FocusModeChangedGameEvent>
    {
    }
}
