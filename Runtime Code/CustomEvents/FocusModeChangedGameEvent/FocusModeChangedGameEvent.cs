using UnityEngine;
using com.FDT.GameEvents;

namespace com.FDT.SystemEventsHandler
{
    [CreateAssetMenu(menuName = "FDT/GameEvents/SystemEventsHandler/FocusModeChangedGameEvent")]
	public class FocusModeChangedGameEvent : GameEvent1<AppFocusMode>
	{
        public override string arg0label
        {
            get { return "Focus"; }
        }
    }
}
