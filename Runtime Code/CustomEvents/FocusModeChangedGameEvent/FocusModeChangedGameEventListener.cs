using com.FDT.GameEvents;
using UnityEngine.Events;

namespace com.FDT.SystemEventsHandler
{
	public class FocusModeChangedGameEventListener : GameEvent1Listener<AppFocusMode, FocusModeChangedGameEvent, FocusModeChangedGameEventListener.UEvt>
	{
        [System.Serializable]
        public class UEvt : UnityEvent<AppFocusMode>
        {
        }
	}
}
