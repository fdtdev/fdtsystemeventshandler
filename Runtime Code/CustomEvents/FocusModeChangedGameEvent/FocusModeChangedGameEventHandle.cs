using com.FDT.GameEvents;

namespace com.FDT.SystemEventsHandler
{
	[System.Serializable]
	public class FocusModeChangedGameEventHandle: GameEvent1Handle<AppFocusMode, FocusModeChangedGameEvent>
	{
	}
}
