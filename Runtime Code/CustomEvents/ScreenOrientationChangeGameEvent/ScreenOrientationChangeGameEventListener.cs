using com.FDT.GameEvents;
using UnityEngine;
using UnityEngine.Events;

namespace com.FDT.SystemEventsHandler
{
	public class ScreenOrientationChangeGameEventListener : GameEvent1Listener<ScreenOrientation, ScreenOrientationChangeGameEvent, ScreenOrientationChangeGameEventListener.UEvt>
	{
        [System.Serializable]
        public class UEvt : UnityEvent<ScreenOrientation>
        {
        }
	}
}
