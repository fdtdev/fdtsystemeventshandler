using UnityEngine;
using com.FDT.GameEvents;

namespace com.FDT.SystemEventsHandler
{
    [CreateAssetMenu(menuName = "FDT/GameEvents/SystemEventsHandler/ScreenOrientationChangeGameEvent")]
	public class ScreenOrientationChangeGameEvent : GameEvent1<ScreenOrientation>
	{
        public override string arg0label
        {
            get { return "Orientation"; }
        }
	}
}
