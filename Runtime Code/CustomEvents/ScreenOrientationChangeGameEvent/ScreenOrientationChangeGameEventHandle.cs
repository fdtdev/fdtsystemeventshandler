using UnityEngine;
using com.FDT.GameEvents;

namespace com.FDT.SystemEventsHandler
{
	[System.Serializable]
	public class ScreenOrientationChangeGameEventHandle: GameEvent1Handle<ScreenOrientation, ScreenOrientationChangeGameEvent>
	{
	}
}
