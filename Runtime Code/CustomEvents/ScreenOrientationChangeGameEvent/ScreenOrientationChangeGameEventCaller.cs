using com.FDT.GameEvents;
using UnityEngine;

namespace com.FDT.SystemEventsHandler
{
	public class ScreenOrientationChangeGameEventCaller: GameEvent1Caller<ScreenOrientation, ScreenOrientationChangeGameEvent>
	{
	}
}
