using com.FDT.GameEvents;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler
{
	[System.Serializable]
	public class ActiveSceneChangedGameEventHandle: GameEvent2Handle<Scene, Scene, ActiveSceneChangedGameEvent>
	{
	}
}
