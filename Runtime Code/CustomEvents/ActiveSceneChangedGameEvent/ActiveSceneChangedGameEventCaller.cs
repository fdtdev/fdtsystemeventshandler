using com.FDT.GameEvents;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler
{
	public class ActiveSceneChangedGameEventCaller: GameEvent2Caller<Scene, Scene, ActiveSceneChangedGameEvent>
	{
	}
}
