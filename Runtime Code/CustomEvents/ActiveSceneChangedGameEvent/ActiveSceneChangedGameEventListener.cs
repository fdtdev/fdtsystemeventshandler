using com.FDT.GameEvents;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler
{
	public class ActiveSceneChangedGameEventListener : GameEvent2Listener<Scene, Scene, ActiveSceneChangedGameEvent, ActiveSceneChangedGameEventListener.UEvt>
	{
        [System.Serializable]
        public class UEvt : UnityEvent<Scene, Scene>
        {
        }
	}
}
