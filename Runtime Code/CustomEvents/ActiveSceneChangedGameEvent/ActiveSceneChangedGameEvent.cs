using UnityEngine;
using com.FDT.GameEvents;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler
{
    [CreateAssetMenu(menuName = "FDT/GameEvents/SystemEventsHandler/ActiveSceneChangedGameEvent")]
	public class ActiveSceneChangedGameEvent : GameEvent2<Scene, Scene>
	{
        public override string arg0label
        {
            get { return "Old Scene"; }
        }

        public override string arg1label
        {
            get { return "New Scene"; }
        }
    }
}
