using UnityEngine;
using com.FDT.GameEvents;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler
{
    [CreateAssetMenu(menuName = "FDT/GameEvents/SystemEventsHandler/SceneLoadGameEvent")]
	public class SceneLoadGameEvent : GameEvent2<Scene, LoadSceneMode>
	{
        public override string arg0label
        {
            get { return "Scene"; }
        }

        public override string arg1label
        {
            get { return "LoadMode"; }
        }
    }
}
