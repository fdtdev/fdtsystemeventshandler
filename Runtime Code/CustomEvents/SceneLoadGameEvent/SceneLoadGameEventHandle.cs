using com.FDT.GameEvents;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler
{
	[System.Serializable]
	public class SceneLoadGameEventHandle: GameEvent2Handle<Scene, LoadSceneMode, SceneLoadGameEvent>
	{
	}
}
