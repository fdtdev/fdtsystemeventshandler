using com.FDT.GameEvents;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler
{
	public class SceneLoadGameEventCaller: GameEvent2Caller<Scene, LoadSceneMode, SceneLoadGameEvent>
	{
	}
}
