using com.FDT.GameEvents;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace com.FDT.SystemEventsHandler
{
	public class SceneLoadGameEventListener : GameEvent2Listener<Scene, LoadSceneMode, SceneLoadGameEvent, SceneLoadGameEventListener.UEvt>
	{
        [System.Serializable]
        public class UEvt : UnityEvent<Scene, LoadSceneMode>
        {
            
        }
	}
}
