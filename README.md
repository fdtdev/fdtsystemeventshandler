# FDT System Events Handler

Basic System Events hook for game events


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.systemeventshandler": "https://bitbucket.org/fdtdev/fdtsystemeventshandler.git#3.0.0",
	"com.fdt.common": "https://bitbucket.org/fdtdev/fdtcommon.git#5.0.0",
	"com.fdt.gameevents": "https://bitbucket.org/fdtdev/fdtgameevents.git#2.0.0",

	...
  }
}

```

## License

MIT - see [LICENSE](https://bitbucket.org/fdtdev/fdtsystemeventshandler/src/3.0.0/LICENSE.md)